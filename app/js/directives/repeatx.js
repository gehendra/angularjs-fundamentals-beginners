/**
 * Created by gehendrakarmacharya on 1/12/17.
 */
'use strict';

eventsApp.directive('repeatX', function($compile){
  return {
    compile: function(element, attributes) {
      for(var i = 0; i < Number(attributes.repeatX)-1; i++) {
        element.after(element.clone().attr('repeat-x', 0));
      }

      return function(scope, element, attrs, controller) {
        attrs.$observe('text', function(newValue, oldValue) {
          if (newValue === 'Hello World') {
            element.css('color', 'red');
          }
        });
      }
    }
    /*
    link: function(scope, element, attrs, controller) {

      for(var i = 0; i < Number(attrs.repeatX)-1; i++) {
        element.after($compile(element.clone().attr('repeat-x', 0))(scope));
      }
    }
    */
  };
});