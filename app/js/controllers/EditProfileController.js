/**
 * Created by gehendrakarmacharya on 1/10/17.
 */
'use strict';

eventsApp.controller('EditProfileController',
  function EditProfileController($scope, gravatarUrlBuilder) {
    $scope.user = {};

    $scope.getGravatarUrl = function(email) {
      return gravatarUrlBuilder.buildGravatarUrl(email);
    };
  }
);