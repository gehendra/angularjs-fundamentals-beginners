/**
 * Created by gehendrakarmacharya on 1/8/17.
 */
'use strict';

eventsApp.controller('EventController', function EventController($scope, eventData, $anchorScroll, $routeParams, $route) {
  //$scope.event = eventData.getEvent($routeParams.eventId);

  $scope.event = $route.current.locals.event

  console.log($route.current.params.eventId);

  $scope.upVoteSession = function(session) {
    session.upVoteCount++;
  }

  $scope.downVoteSession = function(session) {
    session.upVoteCount--;
  }

  $scope.sortorder = "name";

  $scope.scrollToScreen = function() {
    $anchorScroll();
  }

});