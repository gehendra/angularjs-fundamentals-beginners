/**
 * Created by gehendrakarmacharya on 1/11/17.
 */
'use strict';

eventsApp.controller('CompileSampleController',
  function CompileSampleController($scope, $compile, $parse){
  $scope.appendDivToElement = function(markup){
    return $compile(markup)($scope).appendTo(angular.element('#appendHere'));
  }

  var fn = $parse('1 + 2');
  console.log(fn());

  var getter = $parse('event.name');

  var context1 = {event: {name: 'Gehendra'}};
  var context2 = {event: {name: 'Bijay'}};

  console.log(getter(context1));
  console.log(getter(context2));
  console.log(getter(context2, context1));

  var setter = getter.assign;
  setter(context2, 'Whatsup');

  console.log(context2.event.name);

});