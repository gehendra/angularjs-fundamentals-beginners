/**
 * Created by gehendrakarmacharya on 1/11/17.
 */
'use strict';

eventsApp.controller('TimeoutSampleController', function TimeoutSampleController($scope, $timeout){

  var promise = $timeout(function(){
    $scope.name = 'Gehendra Karmacharya';
  }, 3000);


  $scope.cancel = function() {
    $timeout.cancel(promise);
  }
});