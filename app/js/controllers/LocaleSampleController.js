/**
 * Created by gehendrakarmacharya on 1/11/17.
 */
'use strict';

eventsApp.controller('LocaleSampleController', function LocaleSampleController($scope, $locale){

  console.log($locale);
  $scope.date = Date.now();
  $scope.myFormat = $locale.DATETIME_FORMATS.fullDate;

  throw { message: 'error message'};
});