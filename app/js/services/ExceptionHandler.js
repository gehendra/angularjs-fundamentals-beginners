/**
 * Created by gehendrakarmacharya on 1/11/17.
 */
'use strict';

eventsApp.factory('$exceptionHandler', function() {
  return function(exception) {
    console.log('Exception handled: ' + exception.message);
  }
});
