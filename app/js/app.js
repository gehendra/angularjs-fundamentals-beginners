'use strict';

var eventsApp = angular.module('eventsApp', ['ngResource', 'ngRoute'])
                      .config(function($routeProvider, $locationProvider){
                        $routeProvider
                          .when('/newEvent',
                          {
                            templateUrl: 'templates/NewEvent.html',
                            controller: 'EditEventController'
                          })
                          .when('/events',
                            {
                              templateUrl: 'templates/EventList.html',
                              controller: 'EventListController'
                            })
                          .when('/event/:eventId',
                            {
                              templateUrl: 'templates/EventDetails.html',
                              controller: 'EventController',
                              resolve: {
                                event: function( $route, eventData) {
                                  return eventData.getEvent($route.current.pathParams.eventId).$promise;
                                }
                              }
                            })
                            .when('/SampleDirective',
                              {
                                templateUrl: 'templates/SampleDirective.html',
                                controller: 'SampleDirectiveController'
                              })
                          .otherwise({redirectTo: '/events'});
                        $locationProvider.html5Mode(true);
                      })
                      .factory('myCache', ['$cacheFactory',function($cacheFactory){
                        return $cacheFactory('myCache', {capacity: 3})
                      }]);




/*
eventsApp.config(['$resourceProvider', function($resourceProvider){
  $resourceProvider.defaults.stripTrailingSlashes = false;
}]);

eventsApp.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[').endSymbol(']]');
});*/